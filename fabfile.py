import subprocess,os,sys,re
from fabric.api import *
from fabric.colors import *
from datetime import datetime, date, time, timedelta

env.hosts = ['log.csub.edu']

#note: add check for this
env.use_ssh_config = True

#needed for freebsd
env.shell = "/bin/sh -c"

	
@task(alias='s')
def status(): #To check number of errors today & yesterday
	""" - Quickly check the number of timeout/duplicate packets in the last 15/30/60 minutes
	"""

	
	s60 = "T" + (datetime.now() - timedelta(minutes=60)).strftime("%H:%M") # now minus 1 hour
	s30 = "T" + (datetime.now() - timedelta(minutes=30)).strftime("%H:%M") # now minus 1 hour
	s15 = "T" + (datetime.now() - timedelta(minutes=15)).strftime("%H:%M") # now minus 1 hour
	end = "T" + datetime.now().strftime("%H:%M") #now
	today = date.today().strftime("%Y%m%d") #today
	yesterday = (date.today() - timedelta(days=1)).strftime("%Y%m%d") #yesterday
	 
	print("Getting data from log.csub.edu...")	
	with hide('everything'):
		t1 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius2/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s15, end, today)) 
		t2 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius2/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s30, end, today)) 
		t3 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius2/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s60, end, today)) 
		r1 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s15, end, today)) 
		r2 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s30, end, today)) 
		r3 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s60, end, today)) 
	
		print(white("Errors Today"))
		print(white("Radius1: ") + red("%s, " % r1) + yellow("%s, " % r2) + blue("%s " % r3))
		print(white("Radius2: ") + red("%s, " % t1) + yellow("%s, " % t2) + blue("%s " % t3))

		print("Getting data from log.csub.edu...")	

		t1 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius2/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s15, end, yesterday)) 
		t2 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius2/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s30, end, yesterday)) 
		t3 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius2/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s60, end, yesterday)) 
		r1 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s15, end, yesterday)) 
		r2 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s30, end, yesterday)) 
		r3 = run('sed -e \'1,/%s/d\' -e \'/%s/,$d\' /var/log/radius/%s | egrep \'timeout|Discarding duplicate request\' | wc -l ' % (s60, end, yesterday)) 
	
		print(white("Errors yesterday, same timespan:"))
		print(white("Radius1: ") + red("%s, " % r1) + yellow("%s, " % r2) + blue("%s " % r3))
		print(white("Radius2: ") + red("%s, " % t1) + yellow("%s, " % t2) + blue("%s " % t3))
